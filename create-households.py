#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  create-households.py -> distributes household data on populated buildings 
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

import multiprocessing
from lib.polygons import Polygon,Polygons
from lib.centroids import get_centroids
from lib.idw import calc_all_idws
from lib.localise_values import localise_values
from lib.integral_populations import integrate
from lib.pop_per_building import distribute_gridded_population_onto_buildings
from lib.generate_building_blocks import generate_building_blocks
from lib.local_statistics import calculate_local_statistics

################################
# DATA SOURCES AND SINKS
# we are assuming that all input 
# datasets are in the same 
# spatial reference system
################################
# src datasets
src_driver="PostgreSQL"
src_database="pg:dbname=onebyone"
src_census_tracts="BOD_censustracts"
src_buildings="BOD_buildings"
src_popgrid="BOD_popgrid"
src_popgrid_popfield="IND"
src_roads="BOD_roads"

# names for intermediate datasets
int_buildings_with_stats="BOD_i_buildingswithstats"
int_filtered_buildings="BOD_i_buildingsfiltered"
int_pop_per_building="BOD_i_popperbuilding"
int_pop_per_building_popfield="population"
int_buildingblocks="BOD_i_buildingblocks"
int_buildingblocks_with_stats="BOD_i_buildingblockswithstats"

# names for output datasets
#out_

debug=True #False
################################



def create_households():
	
	# a) create building blocks for buildings
	#    the roads dataset is derived from osm roads using: 
	#         create table hel_roads as (select geometry from osm_roads where ST_Intersects(geometry,(SELECT geom from hel_greaterhelsinki limit 1)) and type not in ('service','footway','cycleway','path','track','steps','rail','tram','subway','bridleway','monorail','narrow_gauge') union all select geom from osm_coastlines where ST_Intersects(geom,(SELECT geom from hel_greaterhelsinki limit 1)));
	#    or   create table bor_roads as  (select geometry from osm_roads where ST_Intersects(geometry,(SELECT geom from bor_greaterbordeaux limit 1)) and type not in ('service','footway','cycleway','path','track','steps','rail','tram','subway','bridleway','monorail','narrow_gauge') union all select geom from osm_coastlines where ST_Intersects(geom,(SELECT geom from bor_greaterbordeaux limit 1)));
	#    or a respective command for other cities
	print("---- deriving “building blocks” out of the road network ////")
	print("     - loading road data")
	roads=Polygons()
	roads.load(src_driver,src_database,src_roads,debug=debug)
	print("     - generate building blocks")
	building_blocks=generate_building_blocks(roads,debug=debug)
	print("     - saving results")
	building_blocks.save(src_driver,src_database,int_buildingblocks,debug=debug)
	print("     - cleaning up")
	del roads
	del building_blocks


	# b) calculate areal statistics for buildings, grouped by building blocks
	print("---- calculating areal statistics for buildings, grouped by building blocks ////")
	print("     - loading building blocks")
	building_blocks=Polygons()
	building_blocks.load(src_driver,src_database,int_buildingblocks,debug=debug)
	print("     - loading buildings")
	buildings=Polygons()
	buildings.load(src_driver,src_database,src_buildings,debug=debug)
	print("     - calculate statistics")
	(buildings,building_blocks)=calculate_local_statistics(buildings,building_blocks,src_popgrid,src_popgrid_popfield,debug=debug)
	print("     - saving results")
	buildings.save(src_driver,src_database,int_buildings_with_stats,debug=debug)
	building_blocks.save(src_driver,src_database,int_buildingblocks_with_stats,debug=debug)
	print("     - cleaning up")
	del buildings
	del building_blocks

        return

	# c) tbd: filter outliers (per building block)
	
	# d) tbd: calculate correlation matrix for all columns
	
	# e) evaluate best variable order to fill in values – use part of the data to predict another part, monte-carlo simulation
	

	# f) distribute r72-carroyées population onto buildings 
######################## change input to int_filtered_buildings after implementing the outliers-filter!!!!	
	print("---- distributing gridded population onto buildings ////")
	print("     - loading building polygons from %s://%s %s." % (src_driver,src_database,src_buildings))
	buildings=Polygons()
	buildings.load(src_driver,src_database,src_buildings,debug=debug)
	b_extent=buildings.getExtent()
	print("     - loading population grid from %s://%s %s." % (src_driver,src_database,src_buildings))
	popgrid=Polygons()
	popgrid.load(src_driver,src_database,src_popgrid,spatialfilter="POLYGON((%(west).5f %(north).5f, %(west).5f %(south).5f, %(east).5f %(south).5f, %(east).5f %(north).5f, %(west).5f %(north).5f))"%{"north": b_extent[3], "south": b_extent[1],"west": b_extent[0], "east": b_extent[2]},debug=debug) 
	# spatialfilter="POLYGON((%(west).5f %(north).5f, %(east).5f %(north).5f, %(east).5f %(south).5f, %(west).5f %(south).5f, %(west).5f %(north).5f))"%{"north": b_extent[1], "south": b_extent[0],"west": b_extent[2], "east": b_extent[3]}
	print("     - distributing the population")
	buildings=distribute_gridded_population_onto_buildings(buildings,popgrid,src_popgrid_popfield,int_pop_per_building_popfield,debug=debug)
	print("     - saving results to %s://%s %s." % (src_driver,src_database,int_pop_per_building))
	buildings.save(src_driver,src_database,int_pop_per_building,debug=debug)
	print("     - cleaning up")
	del buildings
	del b_extent
	del popgrid
	return

	# g) load iris polygons (containing all data columns) 
	iris_btx=Polygons()
	iris_btx.load(src_driver,src_database,src_census_tracts)
	#iris_btx.load("SQLite",database,"IRIS_33063_BTX_IC",attributefilter="iris like '08%'",debug=debug) # limit number of iris' for easier debugging (quicker execution)
	print(iris_btx)

	# h) load building polygons
	buildings=Polygons()
	buildings.load(src_driver,src_database,int_pop_per_building,debug=debug)
	print(buildings)

	# i) convert iris and building polygons into centroids (with values attached)
	centroids_iris=get_centroids(iris_btx,debug=debug)
	centroids_buildings=get_centroids(buildings,debug=debug)

	# j) calculate an inverse distance weighted value of all iris-columns for each building (in centroids), using the probabilities and order from d) and e)
	centroids_buildings=calc_all_idws(centroids_buildings,centroids_iris,debug=debug)

	# k) copy newly calculated (idw-)values of buildings from centroids to polygons
	for c in centroids_buildings:
		p=buildings.getPolygonByFID(c["fid"])
		if p is not None:
			p.fields=c["fields"]
		else:
			print("Missed FID %d in buildings" %(c["fid"],))

	# l) normalise idw by total value in respective iris, save buildings
	buildings=localise_values(iris_btx,buildings,debug=debug)
	buildings.save(src_driver,src_database,"btx_batiments",debug=debug)

	# m) temporarily: pre-calculated btx-batiments
	buildings=Polygons()
	buildings.load(src_driver,src_database,"btx_batiments",debug=debug)
	print(buildings)

	buildings=integrate(iris_btx,buildings,debug=debug)

	buildings.save(src_driver,src_database,"btx_batiments_int",debug=debug)





# we will mostly run this modul from within another program. 
# still, directly running it should be possible to enable 
# debugging and development
def main():
	create_households()

if __name__ == '__main__':
	main()
