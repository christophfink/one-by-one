#!/usr/bin/python -u
# -*- coding: utf-8 -*-

# quick and dirty test for lib/polygons.py


from ..lib.polygons import Polygon,Polygons
database="/home/christoph/Dokumente/Dissertation/abm/Daten/Bordeaux/bordeaux-statistische daten/BTX_IC_2008_combined.sqlite"
parcels=Polygons()
parcels.load("SQLite",database,"PARCELLE_EPSG3945")

print repr(parcels)

for p in parcels:
	print repr(p)
	print p.wkt
	print p.fields
	break

