#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  lib/idw
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import os, sys, shapely, shapely.ops, multiprocessing
from osgeo import ogr, osr
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()

#from polygons import Polygon,Polygons

def weight(new_point,known_point):
	return 1/new_point.distance(known_point)

def idw(new_point,known_points,p):
	sum_of_weights=0
	for xi in known_points:
		xi["weight"]=weight(new_point["geom"],xi["geom"])
		sum_of_weights+=xi["weight"]
	sum_of_weights=pow(sum_of_weights,p) # idw-power-parameter

	if not "fields" in new_point:
		new_point["fields"]={}
	if not isinstance(new_point["fields"],type(dict)):
		try:
			new_point["fields"]=dict(new_point["fields"]) # if there is existing data, try to convert it to a dict
		except:
			new_point["fields"]={}

	for v in known_points[0]["fields"].keys():
		if isinstance(known_points[0]["fields"][v],int) or isinstance(known_points[0]["fields"][v],float):
			value=0.0
			for xi in known_points:
				w=xi["weight"]/sum_of_weights
				value+=xi["fields"][v]*w*1.0 # make it float by default
			#new_point["fields"]["idw_"+v]=value
			new_point["fields"][v]=value
	return new_point

def worker(q_in,q_out,known_points,p):
	new_point=q_in.get()
	while new_point!="ALL DONE":
		q_out.put(idw(new_point,known_points,p))
		new_point=q_in.get()
	q_out.put("ALL DONE")

def fill_q_in(points,q_in):
	for p in points:
		q_in.put(p)
	for i in range(multiprocessing.cpu_count()+1):
		q_in.put("ALL DONE")

def collect_from_q_out(q_out,q_final):
	pills=multiprocessing.cpu_count()+1
	items=[]
	while pills>0:
		c=q_out.get()
		if c=="ALL DONE":
			pills-=1
		else:
			items.append(c)
	q_final.put(items)

def calc_all_idws(new_points,known_points,power=4,debug=False):
	if debug: 
		print("Calculating IDWs")
	q_in=multiprocessing.Queue()
	q_out=multiprocessing.Queue()
	q_final=multiprocessing.Queue()

	threads=[]

	p=multiprocessing.Process(target=fill_q_in,args=(new_points,q_in))
	p.start()
	threads.append(p)

	for i in range(multiprocessing.cpu_count()+1):
		p=multiprocessing.Process(target=worker,args=(q_in,q_out,known_points,power))
		p.start()
		threads.append(p)

	p=multiprocessing.Process(target=collect_from_q_out,args=(q_out,q_final))
	p.start()
	#threads.append(p) # would keep hanging, because it waits for the queue to be read

	for t in threads: 
		t.join()

	new_points=q_final.get()
	return new_points




def main():
	print("This module is not meant to be run by YOU :-P \nNo, seriously: use it as a module in other Python programs! Do not run it directly!");


if __name__ == '__main__':
	main()
