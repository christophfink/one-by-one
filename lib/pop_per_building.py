#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  lib/pop_per_building.py
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

from __future__ import print_function

import os, sys, math, shapely, shapely.ops, multiprocessing
from osgeo import ogr, osr, gdal
gdal.ErrorReset()
gdal.PushErrorHandler('CPLQuietErrorHandler')
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()

def distribute_gridded_population_onto_buildings(buildings,popgrid,src_popgrid_popfield="population",int_pop_per_building_popfield="population",debug=False):
	q_in=multiprocessing.Queue(5192)
	q_out=multiprocessing.Queue(5192)
	q_final=multiprocessing.Queue(1)

	threads=[]
	p=multiprocessing.Process(target=fill_q_in,args=(popgrid,q_in))
	p.start()
	threads.append(p)
	
	for i in range(multiprocessing.cpu_count()+1):
		p=multiprocessing.Process(target=worker,args=(q_in,q_out,buildings,src_popgrid_popfield))
		p.start()
		threads.append(p)

	p=multiprocessing.Process(target=collect_from_q_out,args=(q_out,q_final,buildings,int_pop_per_building_popfield))
	p.start()
	threads.append(p)
	
	(buildings,)=q_final.get()
	
	return buildings	

def worker(q_in,q_out,buildings,src_popgrid_popfield):
	while True:
		gridcell=q_in.get()
		if gridcell=="ALL DONE":
			break
		affectedBuildings=[]
		builtUpArea=0
		totalShare=0
		weight_area=20
		weight_share=50

		for b in buildings:
			if gridcell.geom.intersects(b.geom):
				area=gridcell.geom.intersection(b.geom).area
				share=1.0*(area/b.geom.area) # buildings which lie in different grid cells should accumulate to 1.0 area in total
				
				affectedBuildings.append({"geom":b.geom, "area":area, "share": share, "fid": b.fid, "population_integer":0})
				builtUpArea+=area
				totalShare+=share
		if builtUpArea>0:
			for b in affectedBuildings:
				b['population_float']=((b['area']/builtUpArea*weight_area+b['share']/totalShare*weight_share)/(weight_area+weight_share))*gridcell.fields[src_popgrid_popfield]
				b['population_integer']=round(b['population_float'])
				b['remainder']=b['population_float']-math.floor(b['population_float'])
				if b['population_integer']==0: # prevent the next step to create a negative number of people per building by setting remainder highhighhigh
					b['remainder']=99 
			checksum=sum([b['population_integer'] for b in affectedBuildings])
			expectedsum=math.floor(gridcell.fields[src_popgrid_popfield]) # sometimes half persons in stats -> cannot use in model
			while checksum!=expectedsum:
				if checksum<expectedsum:
					largest_remainder=max([b['remainder'] for b in affectedBuildings])
					for b in affectedBuildings:
						if b['remainder']==largest_remainder:
							b['population_integer']+=1
							b['remainder']=-99 # take out of set of largest remainders
							break
				elif checksum>expectedsum:
					smallest_remainder=min([b['remainder'] for b in affectedBuildings])
					for b in affectedBuildings:
						if b['remainder']==smallest_remainder:
							b['population_integer']-=1
							b['remainder']=99 # take out of set of smallest remainders
							break
				checksum=sum([b['population_integer'] for b in affectedBuildings])
			for b in affectedBuildings:
				q_out.put((b['fid'],b['population_integer']))
			#print("sum – calc/expected: %d/%.1f" %(checksum,gridcell.population))
	q_out.put(("ALL DONE",""))
	

def fill_q_in(polygons,q_in):
	for p in polygons:
		q_in.put(p)
	for i in range(multiprocessing.cpu_count()+1):
		q_in.put("ALL DONE")

def collect_from_q_out(q_out,q_final,buildings,int_pop_per_building_popfield): # buildings receive changes in population (function formerly was called updatePopulation, this name is for consistency within the libs-folder)
	for b in buildings:
		b.fields[int_pop_per_building_popfield]=0
	pills=multiprocessing.cpu_count()+1
	while pills>0:
		(fid,change)=q_out.get()
		if fid=="ALL DONE":
			pills-=1
		else:
			buildings.polygons[fid].fields[int_pop_per_building_popfield]+=change
	q_final.put((buildings,))

def tests():
	pass # implement tests here soon ;)

def main():
	print("This module is not meant to be run directly. Running some self-tests instead.")
	tests()

if __name__ == '__main__':
	main()
