#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  lib/centroids
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import os, sys, shapely, shapely.ops, multiprocessing
from osgeo import ogr, osr
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()

from polygons import Polygon,Polygons

def get_centroid(q_in,q_out):
	p=q_in.get()
	while not p=="ALL DONE":
		c={}
		c["geom"]=p.geom.centroid
		c["fields"]=p.fields
		c["fid"]=p.fid
		
		q_out.put(c)
		p=q_in.get()
	c=None
	q_out.put("ALL DONE")

def fill_q_in(polygons,q_in):
	for p in polygons:
		q_in.put(p)
	for i in range(multiprocessing.cpu_count()+1):
		q_in.put("ALL DONE")

def collect_from_q_out(q_out,q_final):
	pills=multiprocessing.cpu_count()+1
	centroids=[]
	while pills>0:
		c=q_out.get()
		if c=="ALL DONE":
			pills-=1
		else:
			centroids.append(c)
	q_final.put(centroids)

def get_centroids(polygons,debug=False):
	if debug: 
		print("Generating centroids for %s" %(repr(polygons),))
	q_in=multiprocessing.Queue()
	q_out=multiprocessing.Queue()
	q_final=multiprocessing.Queue()

	threads=[]

	p=multiprocessing.Process(target=fill_q_in,args=(polygons,q_in))
	p.start()
	threads.append(p)

	for i in range(multiprocessing.cpu_count()+1):
		p=multiprocessing.Process(target=get_centroid,args=(q_in,q_out))
		p.start()
		threads.append(p)

	p=multiprocessing.Process(target=collect_from_q_out,args=(q_out,q_final))
	p.start()
	#threads.append(p) # would keep hanging, because it waits for the queue to be read

	for t in threads: 
		t.join()

	centroids=q_final.get()
	return centroids




def main():
	print("This module is not meant to be run by YOU :-P \nNo, seriously: use it as a module in other Python programs! Do not run it directly!");


if __name__ == '__main__':
	main()
