#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  lib/localise_values  -> normalise calculated idw to local (iris) sum
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import os, sys, shapely, shapely.ops, multiprocessing
from osgeo import ogr, osr
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()

from polygons import Polygon,Polygons

# too general -> a more specific approach is easier
#def sum_of_property_values(items,field):
#	sumopv=0
#	try:
#		for i in items:
#			sumopv+=i[field]
#	except:
#		print("Encountering stiff headwind here, trying to sum up [%s] … " %(field))
#	return sumopv

def sum_of_property_values(buildings,field):
	sumopv=0
	try:
		for b in buildings:
			sumopv+=b.fields[field]
	except:
		print("Encountering stiff headwind here, trying to sum up buildings.fields[%s] … " %(field))
		return -99
	return sumopv

def worker(q_in,q_out,buildings):
	iris=q_in.get()
	while iris!="ALL DONE":
		bldgs_in_iris=buildings.getPolygonsSpatiallyFiltered(iris.geom)
		for v in bldgs_in_iris[0].fields.keys():
			if isinstance(bldgs_in_iris[0].fields[v],int) or isinstance(bldgs_in_iris[0].fields[v],float):
				sumopv=sum_of_property_values(bldgs_in_iris,v)
				try:
					ratio=iris.fields[v]/(sumopv*1.0)
				except:
					continue
					pass # print("Field %s not available in IRIS data." %(v,))
				for b in bldgs_in_iris:
					b.fields[v]*=ratio
		for b in bldgs_in_iris:
				q_out.put(b)
		bldgs_in_iris=None
		iris=q_in.get()
	q_out.put("ALL DONE")

def fill_q_in(iris,q_in):
	for i in iris:
		q_in.put(i)
	for i in range(multiprocessing.cpu_count()+1):
		q_in.put("ALL DONE")

def collect_from_q_out(q_out,q_final):
	pills=multiprocessing.cpu_count()+1
	items=[]
	while pills>0:
		c=q_out.get()
		if c=="ALL DONE":
			pills-=1
		else:
#			if c in items:
			items.append(c)
	q_final.put(items)

def localise_values(iris,buildings,debug=False):
	if debug: 
		print("Calculating local values for %s from %s" %(repr(buildings),repr(iris)))
	q_in=multiprocessing.Queue()
	q_out=multiprocessing.Queue()
	q_final=multiprocessing.Queue()

	threads=[]

	p=multiprocessing.Process(target=fill_q_in,args=(iris,q_in))
	p.start()
	threads.append(p)

	for i in range(multiprocessing.cpu_count()+1):
		p=multiprocessing.Process(target=worker,args=(q_in,q_out,buildings))
		p.start()
		threads.append(p)

	#p=multiprocessing.Process(target=collect_from_q_out,args=(q_out,q_final))
	#p.start()
	#threads.append(p) # would keep hanging, because it waits for the queue to be read

	#for t in threads: 
	#	t.join()

	#bldgs_normalised=q_final.get()

	pills=multiprocessing.cpu_count()+1
	bldgs_normalised=[]
	while pills>0:
		c=q_out.get()
		if c=="ALL DONE":
			pills-=1
		else:
			bldgs_normalised.append(c)

	srs=buildings.srs
	buildings=None
	buildings=Polygons(srs=srs)
	while bldgs_normalised:
		b=bldgs_normalised.pop()
		buildings.addPolygon(Polygon(b.wkt,b.fid,b.fields))
	return buildings




def main():
	print("This module is not meant to be run by YOU :-P \nNo, seriously: use it as a module in other Python programs! Do not run it directly!");


if __name__ == '__main__':
	main()
