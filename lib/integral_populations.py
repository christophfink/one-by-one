#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  lib/integral_populations  -> convert decimal populations to whole-numbers (per building per iris)
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 
import os, sys, shapely, shapely.ops, multiprocessing, operator
from osgeo import ogr, osr
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()

from polygons import Polygon,Polygons

def worker(q_in,q_out,buildings):
	iris=q_in.get()
	while iris!="ALL DONE":
		bldgs_in_iris=buildings.getPolygonsSpatiallyFiltered(iris.geom)
		for v in bldgs_in_iris[0].fields.keys():
			if isinstance(bldgs_in_iris[0].fields[v],float):
				try:
					total=iris.fields[v]
				except:
					print("Field %s not available in IRIS data." %(v,))
					continue
				try:
					bldgs_in_iris.sort(key=operator.methodcaller("getFieldValue",v),reverse=True) # sort from high to low
				except:
					print("Could not sort buildings by field %s" %(v,))
					continue

##### this approach would effectively requiring to work on a copy - but we have to save RAM
#				while total>0 and bldgs_in_iris:
#					b=bldgs_in_iris              # take building with highest value
#					value=b.fields[v] 
#					value=round(value)           # round value
#					if value==0:                 # in case there's still individuals left, but all buildings' values round to 0
#						value=1              # … set the value to 1
#					b.fields[v]=value
#					total-=value                 # substract from total iris pop
#
#				while bldgs_in_iris: # if there are still buildings left after total no is distributed
#					b=bldgs_in_iris
#					b.fields[v]

				for b in range(len(bldgs_in_iris)):				# iterate from high to low
					if total>0:						# if we still have individuals to distribute:
						value=round(bldgs_in_iris[b].fields[v])		#  - round value to whole-number
						if value==0:					#  - in case there's still individuals left, but all buildings' values round to 0
							value=1					#      -> set value to 1
						bldgs_in_iris[b].fields[v]=int(value)		#  - save to polygon instance
						total-=value					#  - update total
					else:
						bldgs_in_iris[b].fields[v]=0

		for b in bldgs_in_iris:
				q_out.put(b)
		bldgs_in_iris=None
		iris=q_in.get()
	q_out.put("ALL DONE")

def fill_q_in(iris,q_in):
	for i in iris:
		q_in.put(i)
	for i in range(multiprocessing.cpu_count()+1):
		q_in.put("ALL DONE")

def collect_from_q_out(q_out,q_final):
	pills=multiprocessing.cpu_count()+1
	items=[]
	while pills>0:
		c=q_out.get()
		if c=="ALL DONE":
			pills-=1
		else:
			items.append(c)
	q_final.put(items)

def integrate(iris,buildings,debug=False):
	#buildings=buildings.returnAsList()
	#buildings.sort(key=operator.methodcaller("getFieldValue","p08_pop1524"))
	#for b in range(len(buildings)):
	#	print buildings[b].fields["p08_pop1524"]
	#return
	if debug: 
		print("Converting values in %s into whole-numbers" %(repr(buildings),))

	q_in=multiprocessing.Queue()
	q_out=multiprocessing.Queue()
	q_final=multiprocessing.Queue()

	threads=[]

	p=multiprocessing.Process(target=fill_q_in,args=(iris,q_in))
	p.start()
	threads.append(p)

	for i in range(multiprocessing.cpu_count()+1):
		p=multiprocessing.Process(target=worker,args=(q_in,q_out,buildings))
		p.start()
		threads.append(p)

	pills=multiprocessing.cpu_count()+1
	bldgs_int=[]
	while pills>0:
		c=q_out.get()
		if c=="ALL DONE":
			pills-=1
		else:
			bldgs_int.append(c)

	srs=buildings.srs
	buildings=None
	buildings=Polygons(srs=srs)
	while bldgs_int:
		b=bldgs_int.pop()
		buildings.addPolygon(Polygon(b.wkt,b.fid,b.fields))
	return buildings




def main():
	print("This module is not meant to be run by YOU :-P \nNo, seriously: use it as a module in other Python programs! Do not run it directly!");


if __name__ == '__main__':
	main()
