#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  lib/pop_per_building.py
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

from __future__ import print_function

import os, sys, math, shapely, shapely.ops, multiprocessing
from osgeo import ogr, osr, gdal
gdal.ErrorReset()
gdal.PushErrorHandler('CPLQuietErrorHandler')
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()

def calculate_local_statistics(buildings,building_blocks,src_popgrid,src_popgrid_popfield,debug=False):
	q_in=multiprocessing.Queue(5192)
	q_out=multiprocessing.Queue(5192)
	q_final=multiprocessing.Queue(1)

	threads=[]
	p=multiprocessing.Process(target=fill_q_in,args=(src_popgrid,q_in))
	p.start()
	threads.append(p)
	
	for i in range(multiprocessing.cpu_count()+1):
		p=multiprocessing.Process(target=worker,args=(q_in,q_out,buildings,src_popgrid_popfield))
		p.start()
		threads.append(p)

	p=multiprocessing.Process(target=collect_from_q_out,args=(q_out,q_final,buildings,int_pop_per_building_popfield))
	p.start()
	threads.append(p)
	
	(buildings,buildingblocks)=q_final.get()
	
	return (buildings,buildingblocks)

def worker(q_in,q_out,buildings,src_popgrid_popfield):
	while True:
		gridcell=q_in.get()
		if gridcell=="ALL DONE":
			break
                # process stuff here ;-)
                affectedBuildings=[] # instead of this :P
		for b in affectedBuildings:
			q_out.put((b['fid'],b['population_integer']))
	q_out.put(("ALL DONE",""))
	

def fill_q_in(polygons,q_in):
	for p in polygons:
		q_in.put(p)
	for i in range(multiprocessing.cpu_count()+1):
		q_in.put("ALL DONE")

def collect_from_q_out(q_out,q_final,buildings,int_pop_per_building_popfield): # buildings receive changes in population (function formerly was called updatePopulation, this name is for consistency within the libs-folder)
	for b in buildings:
		b.fields[int_pop_per_building_popfield]=0
	pills=multiprocessing.cpu_count()+1
	while pills>0:
		(fid,change)=q_out.get()
		if fid=="ALL DONE":
			pills-=1
		else:
			buildings.polygons[fid].fields[int_pop_per_building_popfield]+=change
	q_final.put((buildings,))

def tests():
	pass # implement tests here soon ;)

def main():
	print("This module is not meant to be run directly. Running some self-tests instead.")
	tests()

if __name__ == '__main__':
	main()
