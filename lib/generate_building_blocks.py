#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  lib/generate_building_blocks.py
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

from __future__ import print_function

import os, sys, math, shapely, shapely.ops, multiprocessing
from osgeo import ogr, osr, gdal
gdal.ErrorReset()
gdal.PushErrorHandler('CPLQuietErrorHandler')
from shapely import wkt
from shapely.geometry import LineString, MultiLineString, asMultiLineString
from shapely.ops import unary_union, polygonize

from shapely import speedups
if speedups.available:
	speedups.enable()
	
from polygons import Polygon,Polygons



def generate_building_blocks(roads,debug=False):
#	for r in roads:
#		print("%5d %s"%(len(r.geom.coords),r.geom))
	print(roads.srs)
	buildingBlocks=Polygons(srs=roads.srs,epsg=roads.epsg)
	allLines=[]
	for r in roads:
		if len(r.geom.coords)>1: # no empty lines!
			if type(r.geom)==shapely.geometry.multilinestring.MultiLineString:
				allLines.extend(r.geom)
			elif type(r.geom)==shapely.geometry.linestring.LineString:
				allLines.append(r.geom)
#			print("%5d %s"%(len(r.geom.coords),r.geom))
			#line=asMultiLineString(r.geom)
			#allLines.append(line)
			#print(line.geometryType())
			#allLines.append(asMultiLineString(r.geom))
	#		allLines.extend(line)
#	for l in allLines:
#		print("%5d %s"%(len(l.coords),l))
	allLines=MultiLineString(allLines)
	allLines=unary_union(allLines)
#	return
#	print(type(allLines))
#	print(len(allLines))
#	print(allLines[len(allLines)-1])
#	print(allLines[len(allLines)-1])
#	for l in allLines:
#		print(l)
#	allLines=MultiLineString(allLines)
#	allLines=unary_union(allLines)
	#print(allLines)
	#print(polygonize(allLines))
	polygons=list(polygonize(allLines))
	#print(polygons)
	i=0 # fid
	for p in polygons:
		polygon=Polygon(wkt.dumps(p),i,{"area": p.area})
		buildingBlocks.addPolygon(polygon)
		i+=1
	#print(buildingBlocks)
	#print(buildingBlocks.srs)
	return buildingBlocks


	

def tests():
	pass # implement tests here soon ;)

def main():
	print("This module is not meant to be run directly. Running some self-tests instead.")
	tests()

if __name__ == '__main__':
	main()
