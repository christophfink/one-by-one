#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  helpers/polygons.py
#  
#  Copyright 2012 Christoph Fink <chri@stoph.at>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

from __future__ import print_function

import os, sys, shapely, shapely.ops
from osgeo import ogr, osr
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()
	
from shapely.validation import explain_validity

class Polygon:
	"""
	single polygon
	"""
	def __init__(self,wkt,fid,fields={}):
		self.wkt=wkt
		self.fid=int(fid)
		self.fields=fields
		#self.share=1.0 # when delivered in spatial query, share of overlap can be smaller than 1

		try:
			self.geom=shapely.wkt.loads(wkt)
			self.area=self.geom.area
			self.bounds=self.geom.bounds
		except:
			print("Could not intialise Polygon instance (you may check the geometry ;-) )")

	def __repr__(self):
		return "Polygon feature (FID: %d, field data: %s)" %(self.fid, str(repr(self.fields)))

	def getFieldValue(self,f):
		try:
			return self.fields[f]
		except:
			print("Could not return value from field %s" %(f,))
			#print("Available fields: ")
			#print(self.fields.keys())
			return None

class Polygons:
	"""
	iterable collection of polygons
	"""
	def __init__(self,polygons={},srs=None,epsg=None):
		self.polygons=polygons
		self._index=0
		self._extent=False
		self.srs=srs
		try:
			self.epsg=int(epsg)
		except:
			self.epsg=0
		self.src="[None]"

	def __repr__(self):
		return "Polygon layer, loaded from %s, %d features, EPSG:%d" %(self.src,len(self.polygons),self.epsg)
		
	def addPolygon(self,polygon):
		self.polygons[polygon.fid]=polygon
		self._extent=False
	
	def load(self,ogr_driver,ogr_ds,layername,attributefilter=None,spatialfilter=None,debug=False):
		if debug:
			print("Loading polygons from %s://%s %s." % (ogr_driver,ogr_ds,layername))
		if True: #try:
			#driver=ogr.GetDriverByName(ogr_driver)
			datasource=ogr.Open(ogr_ds, 1)
			layer=datasource.GetLayerByName(layername)

			layer.SetAttributeFilter(attributefilter)
			if spatialfilter is not None:
				layer.SetSpatialFilter(ogr.CreateGeometryFromWkt(spatialfilter))
			self.srs=layer.GetSpatialRef().ExportToProj4()
			self.epsg=layer.GetSpatialRef().GetAttrValue("AUTHORITY", 1)
			
			self.polygons={}
			#self._extent=layer.GetExtent() # different format: (maxx,minx,maxy,miny)
			
			feature = layer.GetNextFeature()
			while feature:

				fields={}
				for f in range(feature.GetFieldCount()):
					f_dfn=feature.GetFieldDefnRef(f)
					# we only process Int, Real and Str
					if f_dfn.GetType()==ogr.OFTReal:
						fields[f_dfn.GetNameRef().upper()]=feature.GetFieldAsDouble(f)
					elif f_dfn.GetType()==ogr.OFTInteger:
						fields[f_dfn.GetNameRef().upper()]=feature.GetFieldAsInteger(f)
					elif f_dfn.GetType()==ogr.OFTString:
						fields[f_dfn.GetNameRef().upper()]=feature.GetFieldAsString(f)

				geom=feature.GetGeometryRef()
				if geom.GetGeometryType() in [ogr.wkbPolygon, ogr.wkbMultiPolygon]:
					geom.CloseRings() # french r72-grid has some not-closed polygons
				polygon=Polygon(geom.ExportToWkt(),feature.GetFID(),fields)
				self.polygons[polygon.fid]=polygon
				feature=layer.GetNextFeature()
				
		#except:
		#	print("Troubles loading polygons from %s://%s %s." % (ogr_driver,ogr_ds,layername))
		#finally:
			feature=None
			layer=None
			datasource=None
			driver=None
			self.src="%s://%s %s" % (ogr_driver,ogr_ds,layername)
	
	def save(self,ogr_driver,ogr_ds,layername,debug=False):
		if debug:
			print("Saving polygons to %s://%s %s." % (ogr_driver,ogr_ds,layername))
		print(self)
		try:
			driver=ogr.GetDriverByName(ogr_driver)
			
			try:
				datasource=driver.Open(ogr_ds,1)
				if datasource is None:
					raise Exception
			except: 
				try:
					try:
						os.makedirs(os.path.dirname(ogr_ds))
					except:
						pass
					datasource=driver.CreateDataSource(ogr_ds)
					if datasource is None:
						raise Exception
				except: 
					print("Sorry pal, but you gave some terribly bogus path name for saving %s to: %s://%s" %(layername,ogr_driver,ogr_ds))
			if(datasource.GetLayerByName(layername)):
				if ogr_driver=='PostgreSQL' or ogr_ds[:2]=="pg": # DeleteLayer doesn't work for PG …
					print(datasource.ExecuteSQL('DROP TABLE "%s";' %(layername)))
				else:   
					datasource.DeleteLayer(layername)

			srs=osr.SpatialReference()
			#srs.ImportFromProj4(self.srs)
			srs.ImportFromEPSG(int(self.epsg))
			#srs.AutoIdentifyEPSG()
			#print(srs.GetAttrValue("AUTHORITY", 1))
			#epsg=str(srs.GetAttrValue("AUTHORITY", 1))
			#if srs.epsg==None and self.srs=="+proj=merc +lon_0=0 +lat_ts=0 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +units=m +no_defs": # for whatever reason, ogr/proj cannot find 900913 or 3857 or 3587
			#	epsg=900913
			#print(srs)


			layer=datasource.CreateLayer(layername, geom_type=ogr.wkbPolygon, srs=srs, options=["OVERWRITE=YES"]) #,"SRID="+str(self.epsg)]) #"FORMAT=SPATIALITE"]) #options=["SPATIAL_INDEX=YES", "SRID="+epsg])
			if ogr_driver=='PostgreSQL' or ogr_ds[:2]=="pg": # somehow pg does not accept the srs-option of createlayer … (upon closer inspection i suspect that ogr is not able to fit the proj4 of epsg3945 to a postgis-srid, which is why it just creates a new one, with one id higher than existing ->900914 – still causes tremendous problems, this here is better)
			    datasource.ExecuteSQL("SELECT UpdateGeometrySRID('%s','wkb_geometry','%04d');" %(layername,self.epsg))
			    layer.SyncToDisk()
			    layer=None
			    layer=datasource.GetLayerByName(layername)
			    datasource.ExecuteSQL("SELECT Find_SRID('public','%s','wkb_geometry');" %(layername,))
			
			fields=[]
			for f in self.polygons.values()[0].fields:
				if isinstance(self.polygons.values()[0].fields[f], int):
					fields.append({"name":f, "ogrType": ogr.OFTInteger, "cast": int})
				elif isinstance(self.polygons.values()[0].fields[f], float):
					fields.append({"name":f, "ogrType": ogr.OFTReal, "cast": float})
				elif isinstance(self.polygons.values()[0].fields[f], str):
					fields.append({"name":f, "ogrType": ogr.OFTString, "cast": str})
			for fd in fields:
				layer.CreateField(ogr.FieldDefn(fd['name'],fd['ogrType']))
			
			featureDefn=layer.GetLayerDefn()
			
			layer.StartTransaction()
			i=0
			for b in self.polygons:
				feature=ogr.Feature(featureDefn)
				feature.SetGeometry(ogr.CreateGeometryFromWkt(self.polygons[b].wkt))
				for f in fields:
					feature.SetField(f['name'].upper(),f['cast'](self.polygons[b].fields[f['name']]))
				try:
					if layer.CreateFeature(feature) != 0:
						raise Exception
					
				except:
					print("Polygon could not be saved:")
					print(repr(feature))
					print(self.polygons[b].wkt)
					print(feature.GetGeometryRef().GetGeometryType())
					print(feature.GetGeometryRef().GetGeometryName())
					print(explain_validity(shapely.wkt.loads(self.polygons[b].wkt)))
				i+=1
				if i>65536:
					layer.CommitTransaction()
			layer.CommitTransaction()
			layer.SyncToDisk()
			print("saved %d polygons" %(i,))
				
		except:
			print("Some weird things are going on while saving polygons to %s://%s %s. You may want to take a look at it …" % (ogr_driver,ogr_ds,layername))
		finally:
			driver=None
			datasource=None
			srs=None
			layer=None
			featureDefn=None
			feature=None
	
	def getExtent(self):
		if self._extent:
			return self._extent
		if len(self.polygons)>0:
			minx=min([self.polygons[b].geom.bounds[0] for b in self.polygons])
			miny=min([self.polygons[b].geom.bounds[1] for b in self.polygons])
			maxx=max([self.polygons[b].geom.bounds[2] for b in self.polygons])
			maxy=max([self.polygons[b].geom.bounds[3] for b in self.polygons])
			self._extent=(minx,miny,maxx,maxy)
			return self._extent
		else:
			return (0,0,0,0)

	def getPolygonByFID(self,fid):
		for p in self.polygons:
			if self.polygons[p].fid==fid:
				#p.share=1.0 # reset share, just in case
				return self.polygons[p]
		return None

	def getPolygonsSpatiallyFiltered(self, spatialfilter):
		if isinstance(spatialfilter,str):
			try:
				spatialfilter=shapely.wkt.loads(spatialfilter)
			except:
				return []
		ps=[]
		for p in self.polygons:
			if spatialfilter.intersects(self.polygons[p].geom):
				#p.share=spatialfilter.intersect(p.geom).area/p.geom.area
				ps.append(self.polygons[p])
		return ps

	def next(self):
		self._index+=1
		if self._index >= len(self.polygons):
			raise StopIteration
		else:
			#self.polygons[self._index].share=1.0 # reset share, just in case
			return self.polygons[self._index]
	
	def __iter__(self):
		return iter(self.polygons.values()) #.iteritems()
		#return iter(self.polygons)
		#return self
		
	def __len__(self):
		return len(self.polygons)
		
	def returnAsList(self):
		return self.polygons.values()
		
	def returnAsDict(self):
		#for p in self.polygons:
		#	p.share=1.0 # reset share, just in case
		return self.polygons


def main():
	print("This module is not meant to be run by YOU :-P \nNo, seriously: use it as a module in other Python programs! Do not run it directly!");


if __name__ == '__main__':
	main()
