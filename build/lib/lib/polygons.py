#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  helpers/polygons.py
#  
#  Copyright 2012 Christoph Fink <chri@stoph.at>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

import os, sys, math, shapely, shapely.ops, multiprocessing
from osgeo import ogr, osr
from shapely import wkt

from shapely import speedups
if speedups.available:
	speedups.enable()

class Polygon:
	"""
	single polygon
	"""
	def __init__(self,wkt,fid,fields={}):
		self.wkt=wkt
		self.fid=int(fid)
		self.fields=fields

		try:
			self.geom=shapely.wkt.loads(wkt)
			self.area=self.geom.area
			self.bounds=self.geom.bounds
		except:
			print("Could not intialise Polygon instance (you may check the geometry ;-) )")

	def __repr__(self):
		return "Polygon feature (FID: %d, field data: %s)" %(self.fid, str(repr(self.fields)))

class Polygons:
	"""
	iterable collection of polygons
	"""
	def __init__(self,polygons={},srs=None):
		self.polygons=polygons
		self._index=0
		self._extent=False
		self.srs=srs
		self.src=""

	def __repr__(self):
		return "Polygon layer, loaded from %s, %d features" %(self.src,len(self.polygons))
		
	def addPolygon(self,polygon):
		self.polygons[polygon.fid]=polygon
		self._extent=False
	
	def load(self,ogr_driver,ogr_ds,layername,attributefilter=None,spatialfilter=None):
		try:
			driver=ogr.GetDriverByName(ogr_driver)
			datasource=driver.Open(ogr_ds, 1)
			layer=datasource.GetLayerByName(layername)

			layer.SetAttributeFilter(attributefilter)
			layer.SetSpatialFilter(spatialfilter)
			
			self.srs=layer.GetSpatialRef().ExportToProj4()
			
			self.polygons={}
			#self._extent=layer.GetExtent() # different format: (maxx,minx,maxy,miny)
			
			feature = layer.GetNextFeature()
			while feature:

				fields={}
				for f in range(feature.GetFieldCount()):
					f_dfn=feature.GetFieldDefnRef(f)
					# we only process Int, Real and Str
					if f_dfn.GetType()==ogr.OFTReal:
						fields[f_dfn.GetName()]=feature.GetFieldAsDouble(f)
					elif f_dfn.GetType()==ogr.OFTInteger:
						fields[f_dfn.GetName()]=feature.GetFieldAsInteger(f)
					elif f_dfn.GetType()==ogr.OFTString:
						fields[f_dfn.GetName()]=feature.GetFieldAsString(f)

				polygon=Polygon(feature.GetGeometryRef().ExportToWkt(),feature.GetFID(),fields)
				self.polygons[polygon.fid]=polygon
				feature=layer.GetNextFeature()
						
		except:
			print("Troubles loading polygons from %s://%s %s." % (ogr_driver,ogr_ds,layername))
		finally:
			feature=None
			layer=None
			datasource=None
			driver=None
			self.src="%s://%s %s." % (ogr_driver,ogr_ds,layername)
	
	def save(self,ogr_driver,ogr_ds,layername):
		try:
			driver=ogr.GetDriverByName(ogr_driver)
			
			#if os.path.exists(filename):
			#	driver.DeleteDataSource(filename)
			
			try:
				datasource=driver.Open(ogr_ds,1)
			except: 
				try:
					driver.DeleteDataSource(ogr_ds)
					datasource=driver.Open(ogr_ds,1)
				except: 
					try:
						os.makedirs(os.path.dirname(ogr_ds))
						datasource=driver.Open(ogr_ds,1)
					except:
						print("Sorry pal, but you gave some terribly bogus path name for saving %s to: %s://%s" %(layername,ogr_driver,ogr_ds))

			if(datasource.GetLayerByName(layername)):
				datasource.DeleteLayer(layername)

			srs=osr.SpatialReference()
			srs.ImportFromProj4(self.srs)

			layer=datasource.CreateLayer(layername, geom_type=ogr.wkbPolygon, srs=srs)
			
			fields=[]
			for f in self.polygons.values()[0].fields:
				if isinstance(self.polygons.values()[0].fields[f], int):
					fields.append({"name":f, "ogrType": ogr.OFTInteger, "cast": int})
				elif isinstance(self.polygons.values()[0].fields[f], float):
					fields.append({"name":f, "ogrType": ogr.OFTReal, "cast": float})
				elif isinstance(self.polygons.values()[0].fields[f], str):
					fields.append({"name":f, "ogrType": ogr.OFTString, "cast": str})
				for fd in fields:
					layer.CreateField(ogr.FieldDefn(fd['name'],fd['ogrType']))
			
			featureDefn=layer.GetLayerDefn()
			
			for b in self.polygons:
				feature=ogr.Feature(featureDefn)
				feature.SetGeometry(ogr.CreateGeometryFromWkt(self.polygons[b].wkt))
				for f in fields:
					feature.SetField(f['name'],f['cast'](self.polygons[b].fields[f['name']]))
				try:
					layer.CreateFeature(feature)
				except:
					print("Polygon could not be saved:")
					print(repr(feature))
					print(self.polygon[b].wkt)
		except:
			print("Some weird ?+#$ going on while saving polygons to %s://%s %s. You might take a look at it …" % (ogr_driver,ogr_ds,layername))
		finally:
			driver=None
			datasource=None
			srs=None
			layer=None
			featureDefn=None
			feature=None
	
	def getExtent(self):
		if self._extent:
			return self._extent
		if len(self.polygons)>0:
			minx=min([self.polygons[b].geom.bounds[0] for b in self.polygons])
			miny=min([self.polygons[b].geom.bounds[1] for b in self.polygons])
			maxx=max([self.polygons[b].geom.bounds[2] for b in self.polygons])
			maxy=max([self.polygons[b].geom.bounds[3] for b in self.polygons])
			self._extent=(minx,miny,maxx,maxy)
			return self._extent
		else:
			return (0,0,0,0)

	def next(self):
		self._index+=1
		if self._index >= len(self.polygons):
			raise StopIteration
		else:
			return self.polygons[self._index]
	
	def __iter__(self):
		return iter(self.polygons.values()) #.iteritems()
		#return self
		
	def __len__(self):
		return len(self.polygons)
		
	def returnAsList(self):
		return self.polygons.values()
		
	def returnAsDict(self):
		return self.polygons
